#include "main.h"
#include <filesystem>
#include <cfiletext.h>
#include <regex>

AsiPlugin::AsiPlugin() : SRDescent() {
	// Constructor
	std::filesystem::path path( std::filesystem::current_path().string() + "/cleo/ctrace" );
	if ( !std::filesystem::exists( path ) || !std::filesystem::is_directory( path ) ) {
		if ( !std::filesystem::is_directory( path ) ) std::filesystem::remove( path );
		std::filesystem::create_directory( path );
	}

	if ( std::filesystem::exists( path.string() + "/SASCM.ini" ) ) {
		CFileText		 txt( "cleo/ctrace/SASCM.ini" );
		std::smatch		 sm;
		const std::regex validator( R"(([0-9a-f]{4})=-?\d+\,)", std::regex::icase );
		for ( auto &line : txt.data() ) {
			if ( !std::regex_search( line, sm, validator ) ) continue;
			sascm[std::stoll( sm[1].str(), 0, 16 )] = line;
		}
	}

	Process.install();
	ProcessOnce.install();
	Shutdown.install();
	ReadString.install( 4 );
	Strncpy.install( 4 );
	ReadString9.install();
	ReadStringE.install();
	ReadStringF.install();
	ReadStringF_2.install();
	CollectEntry.install( 4 );
	CollectReadType.install();
	CollectExit.install();
	CollectNoIncReadType.install();
	CollectNoIncExit.install();
	Store.install( 4 );
	IndexGVar.install();
	VarPointer.install();

	Process.onAfter += [this]( SRHook::CPU &cpu ) {
		processLazy();

		thrd	= (CRunningScript *)cpu.ESI; // thread
		auto op = cpu.AX;					 // opcode
		opId	= ( op & 0x7FFF );

		openFile();
		if ( traces[thrd].is_open() ) traces[thrd] << SRString::format( "{%04X} ", thrd->CurrentIP - thrd->BaseIP );
		opFmt( sascm[opId], op );
	};
	ProcessOnce.onAfter += [this]( SRHook::CPU &cpu ) {
		processLazy();

		thrd	= (CRunningScript *)cpu.ECX; // thread
		auto op = cpu.AX;					 // opcode
		opId	= ( op & 0x7FFF );
		openFile( true );
		if ( traces[thrd].is_open() ) traces[thrd] << SRString::format( "{%04X} ", thrd->CurrentIP - thrd->BaseIP );
		opFmt( sascm[opId], op );
	};
	Shutdown.onBefore += [this]( SRHook::CPU &cpu ) {
		processLazy();
		auto thrd = (CRunningScript *)cpu.ECX;
		if ( !traces[thrd].is_open() ) traces[thrd].close();
	};
	ReadString.onBefore += [this]( char *&buf, int &len ) {
		processLazyString();
		textBuf	   = buf;
		textBufLen = len;
	};
	Strncpy.onBefore += [this]( char *&buf, char *&src, int &len ) {
		if ( textBuf && textBuf == buf ) {
			textBufLen = len;
			processLazyString();
		}
	};
	ReadString9.onBefore += std::tuple{ this, &AsiPlugin::processLazyString };
	ReadStringE.onBefore += [this]( SRHook::CPU &cpu ) {
		if ( cpu.EAX < textBufLen ) textBufLen = cpu.EAX;
		processLazyString();
	};
	ReadStringF.onBefore += std::tuple{ this, &AsiPlugin::processLazyString };
	ReadStringF_2.onBefore += std::tuple{ this, &AsiPlugin::processLazyString };
	CollectEntry.onBefore += [this]( short &count ) {
		readParams = count;
		typeId	   = 0;
	};
	CollectReadType.onAfter += [this]( SRHook::CPU &cpu ) { readTypes[typeId++] = cpu.DL; };
	CollectExit.onBefore += [this]( SRHook::CPU &cpu ) {
		if ( !cpu.ZF ) return;
		for ( int i = 0; i < readParams; ++i ) opFmt.push( params[i], readTypes[i] );
	};
	CollectNoIncReadType.onAfter += [this]( SRHook::CPU &cpu ) { readType = cpu.DL; };
	CollectNoIncExit.onBefore += [this]( SRHook::CPU &cpu ) {
		if ( !traces[thrd].is_open() ) return;
		param.Uint = cpu.EAX;
		opFmt.push( param, readType );
	};
	Store.onBefore += [this]( short &count ) {
		if ( !traces[thrd].is_open() ) return;
		for ( int i = 0; i < count; ++i ) opFmt.push( param );
	};
	IndexGVar.onBefore += [this]( SRHook::CPU &cpu ) {
		if ( !traces[thrd].is_open() ) return;
		auto self = (CRunningScript *)cpu.ECX;
		auto ip	  = self->CurrentIP;
		auto type = *(byte *)( ip++ );
		if ( type == 2 )
			opFmt.push( *(short *)ip, 0x7F );
		else if ( type == 7 ) {
			auto id = *( (short *)ip + 1 );
			ip += 2;
			auto arrId = *( (short *)ip + 1 );
			auto arr   = *( (short *)ip - 1 );
			if ( arrId >= 0 ) {
				if ( self->bIsMission )
					arrId = ( (size_t *)0xA48960 )[id];
				else
					arrId = self->LocalVar[id];
			} else
				size_t arrId = ( (size_t *)0xA49960 )[id];
			opFmt.push( arr + 4 * arrId, 0x7F );
		} else
			opFmt.push( self, 0x7F );
	};
	VarPointer.onAfter += [this]( SRHook::CPU &cpu ) {
		if ( !traces[thrd].is_open() ) return;
		auto	type = cpu.BL;
		size_t *ptr	 = nullptr;
		switch ( type ) {
			case 2:
				[[fallthrough]];
			case 0xA:
				[[fallthrough]];
			case 0x10: {
				auto id = *(short *)thrd->CurrentIP;
				ptr		= &( (size_t *)0xA49960 )[id];
				break;
			}
			case 3:
				[[fallthrough]];
			case 0xB:
				[[fallthrough]];
			case 0x11: {
				auto id = *(short *)thrd->CurrentIP;
				if ( thrd->bIsMission )
					ptr = &( (size_t *)0xA48960 )[id];
				else
					ptr = (size_t *)&thrd->LocalVar[id];
				break;
			}
			default:
				break;
		}
		opFmt.push( ptr, 0x7E );
	};
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	VarPointer.remove();
	IndexGVar.remove();
	Store.remove();
	CollectNoIncExit.remove();
	CollectNoIncReadType.remove();
	CollectExit.remove();
	CollectReadType.remove();
	CollectEntry.remove();
	ReadStringF_2.remove();
	ReadStringF.remove();
	ReadStringE.remove();
	ReadString9.remove();
	Strncpy.remove();
	ReadString.remove();
	Shutdown.remove();
	ProcessOnce.remove();
	Process.remove();

	for ( auto &&[thdr, stream] : traces )
		if ( stream.is_open() ) stream.close();
}

void AsiPlugin::openFile( bool once ) {
	if ( !traces[thrd].is_open() ) {
		std::filesystem::path path( SRString::format( "cleo/ctrace/%08X.trace%s", thrd, ( once ? "_once" : "" ) ) );
		++counter[thrd];
		while ( std::filesystem::exists( path ) )
			path = SRString::format( "cleo/ctrace/%08X(%d).trace%s", thrd, counter[thrd]++, ( once ? "_once" : "" ) );
		traces[thrd].open( path.string() );
	}
}

void AsiPlugin::processLazy() {
	processLazyString();
	printOpcode();
}

void AsiPlugin::processLazyString() {
	static char buf[256]{ 0 };
	if ( textBuf && traces[thrd].is_open() ) {
		memcpy( buf, textBuf, textBufLen );
		buf[textBufLen] = 0;
		for ( int i = 0; i < textBufLen; ++i ) {
			if ( buf[i] < ' ' && buf[i] != '\t' && buf[i] != '\r' && buf[i] != '\n' ) {
				buf[i] = 0;
				break;
			}
		}
		opFmt.push( buf, 0xE );
		textBuf = nullptr;
	}
}

void AsiPlugin::printOpcode() {
	if ( !opFmt.opcode.empty() && opFmt.opcode.find( "FFFF: " ) != 0 && traces[thrd].is_open() )
		traces[thrd] << opFmt.opcode << std::endl;
	opFmt.opcode.clear();
}
