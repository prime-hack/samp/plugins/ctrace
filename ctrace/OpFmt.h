#ifndef OPFMT_H
#define OPFMT_H

#include <cstdint>
#include <sys/SRString.hpp>

union CScriptVar {
	size_t	Uint;
	ssize_t Int;
	float	Float;
};

class OpFmt {
	int ctr = 0;
	int max = -1;

public:
	std::string opcode;

	OpFmt();
	OpFmt( std::string raw, uint16_t op = 0 );
	void operator()( std::string raw, uint16_t op = 0 );

	bool push( CScriptVar var, uint8_t type = 0 );
	bool push( size_t value, uint8_t type = 0x7E );
	bool push( ssize_t value, uint8_t type = 1 );
	bool push( float value, uint8_t type = 6 );
	bool push( void *value, uint8_t type = 0x7F );
};

#endif // OPFMT_H
