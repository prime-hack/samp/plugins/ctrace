#include "OpFmt.h"

OpFmt::OpFmt() {
	opcode.clear();
	max = 0;
	ctr = 0;
}

OpFmt::OpFmt( std::string raw, uint16_t op ) {
	operator()( raw, op );
}

void OpFmt::operator()( std::string raw, uint16_t op ) {
	if ( raw.length() < 8 || raw.find( "," ) == std::string::npos )
		raw = SRString::format( "%04X=-1,UNKNOWN OPCODE", op );

	if ( op != 0 )
		opcode = SRString::format( "%04X: ", op );
	else {
		opcode = raw.substr( 0, 4 );
		opcode += ": ";
	}

	auto comapos = raw.find( "," );
	max			 = std::stoi( raw.substr( 5, comapos ).data() );

	opcode += raw.substr( comapos + 1 );
	while ( opcode.back() == '\n' || opcode.back() == '\r' || opcode.back() == '\t' || opcode.back() == ' ' )
		opcode.pop_back();

	ctr = 0;
}

bool OpFmt::push( CScriptVar var, uint8_t type ) {
	if ( opcode.empty() ) return false;
	if ( ++ctr > max && max != -1 ) return false;
	auto inspos = std::string::npos;
	auto insfmt = SRString::format( "%%%dp%%", ctr );
	inspos		= opcode.find( insfmt );
	if ( inspos == std::string::npos ) {
		insfmt = SRString::format( "%%%dd%%", ctr );
		inspos = opcode.find( insfmt );
		if ( inspos == std::string::npos ) {
			insfmt = SRString::format( "%%%do%%", ctr );
			inspos = opcode.find( insfmt );
			if ( inspos == std::string::npos ) {
				insfmt = SRString::format( "%%%dg%%", ctr );
				inspos = opcode.find( insfmt );
				if ( inspos == std::string::npos ) {
					insfmt = SRString::format( "%%%dx%%", ctr );
					inspos = opcode.find( insfmt );
					if ( inspos == std::string::npos ) {
						insfmt = SRString::format( "%%%ds%%", ctr );
						inspos = opcode.find( insfmt );
						if ( inspos == std::string::npos ) {
							insfmt = SRString::format( "%%%dh%%", ctr );
							inspos = opcode.find( insfmt );
							if ( inspos == std::string::npos && max != -1 )
								return false;
							else if ( inspos != std::string::npos )
								type = 0x7C;
						} else
							type = 0xE;
					}
				}
			}
		}
	} else
		type = 0x7D;
	std::string ins;
	switch ( type ) {
		case 4:
			[[fallthrough]];
		case 5:
			[[fallthrough]];
		case 1:
			ins = SRString::format( "%d", var.Int );
			break;
		case 6:
			ins = SRString::format( "%f", var.Float );
			break;
		case 0x7F:
			ins = SRString::format( "gi{%d}", var.Int );
			break;
		case 0x7E:
			ins = SRString::format( "vo{0x%08X}", var.Uint );
			break;
		case 0x7D:
			ins = SRString::format( "@offset_%08X", ( var.Int < 0 ? var.Int * -1 : var.Int ) );
			break;
		case 0x7C:
			ins = SRString::format( "0x%X", var.Uint );
			break;
		case 0xE:
			ins = SRString::format( "\"%s\"", (char *)var.Uint );
			break;
		default:
			ins = SRString::format( "{0x%X|%d|%.3f}", var.Uint, var.Int, var.Float );
			break;
	}
	if ( inspos != std::string::npos )
		opcode = SRString::replaceOne( opcode, insfmt, ins );
	else
		opcode += " " + ins;
	return true;
}

bool OpFmt::push( size_t value, uint8_t type ) {
	CScriptVar var;
	var.Uint = value;
	return push( var, type );
}

bool OpFmt::push( ssize_t value, uint8_t type ) {
	CScriptVar var;
	var.Int = value;
	return push( var, type );
}

bool OpFmt::push( float value, uint8_t type ) {
	CScriptVar var;
	var.Float = value;
	return push( var, type );
}

bool OpFmt::push( void *value, uint8_t type ) {
	CScriptVar var;
	var.Uint = (size_t)value;
	return push( var, type );
}
