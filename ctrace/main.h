#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <sys/SRDescent.h>
#include <SRHook.hpp>
#include <fstream>
#include <map>
#include "OpFmt.h"

#pragma pack( push, 1 )
struct CRunningScript {
	byte   gap[16];
	size_t BaseIP;
	size_t CurrentIP;
	byte   gap2[36];
	int	   LocalVar[34];
	byte   gap3[24];
	char   bIsMission;
};
#pragma pack( pop )

class AsiPlugin : public SRDescent {
	SRHook::Hook<>						Process{ 0x469FBC, 6 };
	SRHook::Hook<>						ProcessOnce{ 0x469EBC, 6 };
	SRHook::Hook<>						Shutdown{ 0x465AA0, 7 };
	SRHook::Hook< char *, int >			ReadString{ 0x463D50, 6 };
	SRHook::Hook< char *, char *, int > Strncpy{ 0x821F40, 5 };
	SRHook::Hook<>						ReadString9{ 0x463D8F, 6 };
	SRHook::Hook<>						ReadStringE{ 0x463F03, 6 };
	SRHook::Hook<>						ReadStringF{ 0x463F9F, 6 };
	SRHook::Hook<>						ReadStringF_2{ 0x463F7A, 5 };
	SRHook::Hook< short >				CollectEntry{ 0x464080, 5 };
	SRHook::Hook<>						CollectReadType{ 0x4640A3, 6 };
	SRHook::Hook<>						CollectExit{ 0x464214, 6 };
	SRHook::Hook<>						CollectNoIncReadType{ 0x46425A, 5 };
	SRHook::Hook<>						CollectNoIncExit{ 0x464285, 5 };
	SRHook::Hook< short >				Store{ 0x464370, 5 };
	SRHook::Hook<>						IndexGVar{ 0x464700, 6 };
	SRHook::Hook<>						VarPointer{ 0x464798, 6 };

	std::map< CRunningScript *, std::ofstream > traces;
	std::map< CRunningScript *, size_t >		counter;
	std::map< short, std::string >				sascm;

	CRunningScript *thrd;
	uint16_t		opId;
	char *			textBuf = nullptr;
	int				textBufLen;
	short			readParams;
	short			typeId = 0;
	byte			readTypes[32]{ 0 };
	CScriptVar *	params = (CScriptVar *)0xA43C78;
	byte			readType;
	CScriptVar		param;
	OpFmt			opFmt;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void openFile( bool once = false );

	void processLazy();
	void processLazyString();

	void printOpcode();
};

#endif // MAIN_H
